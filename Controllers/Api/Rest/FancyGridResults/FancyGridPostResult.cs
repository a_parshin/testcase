﻿using OrangeTest.Models;

namespace OrangeTest.Controllers.Api.Rest.FancyGridResults
{
    public class FancyGridPostResult
    {
        public FancyGridPostResult(int employee_id)
        {
            this.data = new Employee { id = employee_id };
            this.message = employee_id > 0 ? "Created new User" : "error";
            this.success = employee_id > 0;
        }

        public Employee data;
        public string message;
        public bool success;
    }
}
