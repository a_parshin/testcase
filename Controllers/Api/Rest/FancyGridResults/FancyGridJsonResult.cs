﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace OrangeTest.Controllers.Api.Rest.FancyGridResults
{
    public class FancyGridGetResult<T> : ContentResult
    {
        public FancyGridGetResult(IEnumerable<T> data)
        {
            this.data = data;
            this.start = 0;
            this.success = data != null;

            this.totalCount = 0;
            using (var i = data.GetEnumerator())
            {
                while (i.MoveNext())
                    this.totalCount++;
            }
        }

        public IEnumerable<T> data;
        public int start;
        public bool success;
        public int totalCount;
    }
}
