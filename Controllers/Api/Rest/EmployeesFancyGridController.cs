using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrangeTest.Data;
using OrangeTest.Models;
using OrangeTest.Controllers.Api.Rest.FancyGridResults;
using OrangeTest.Utils;

namespace OrangeTest.Controllers.Api.Rest
{
    [Produces("application/json")]
    [Route("api/EmployeeFancyGrid")]
    public class EmployeeFancyGridController : Controller
    {
        private readonly TestContext _context;

        public EmployeeFancyGridController(TestContext context)
        {
            _context = context;
        }

        // GET: api/EmployeeFancyGrid
        [HttpGet]
        public IActionResult GetEmployees()
        {
            return Ok(new FancyGridGetResult<Employee>(_context.Employees));
        }

        // GET: api/EmployeeFancyGrid/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmployee([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var employee = await _context.Employees.SingleOrDefaultAsync(m => m.id == id);

            if (employee == null)
            {
                return NotFound();
            }

            return Ok(employee);
        }

        // PUT: api/EmployeeFancyGrid/5
        [HttpPut]
        public async Task<IActionResult> PutEmployee(/*[FromBody] int id, [FromBody] string key, [FromBody] string value*/)
        {
            // a workaround since FancyGrid is sending wrong Content-Type
            int id = System.Convert.ToInt32(Request.Form["id"]);
            string key = Request.Form["key"];
            string value = Request.Form["value"];
            string action = Request.Form["action"];

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //_context.Entry(employee).State = EntityState.Modified;

            try
            {
                var employee = SelectEmployee(id);
                _context.Employees.Attach(employee);

                ReflectionUtils.SetProperty(employee, key, value);

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        // POST: api/EmployeeFancyGrid
        [HttpPost]
        public async Task<IActionResult> PostEmployee(/*[FromBody] Employee employee*/)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var employee = new Employee();
            _context.Employees.Add(employee);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetEmployee", new { id = employee.ID }, employee);
            return Ok(new FancyGridPostResult(employee.id));
        }

        // DELETE: api/EmployeeFancyGrid/5
        [HttpDelete()]
        public async Task<IActionResult> DeleteEmployee(/*[FromBody] Employee arg*/)
        {
            // a workaround since FancyGrid is sending wrong Content-Type
            int id = System.Convert.ToInt32(Request.Form["id"]);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var employee = await _context.Employees.SingleOrDefaultAsync(m => m.id == id);
            if (employee == null)
            {
                return NotFound();
            }

            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();

            return Ok(employee);
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.id == id);
        }

        private Employee SelectEmployee(int id)
        {
            return _context.Employees.Single(c => c.id == id);
        }
    }
}