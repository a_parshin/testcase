﻿using System;
using System.Reflection;

namespace OrangeTest.Utils
{
    public static class ReflectionUtils
    {
        public static void SetProperty<T>(T target, string name, object value)
        {
            var prop = target.GetType().GetProperty(name);

            object converted_property_value = null;
            if (prop.PropertyType == typeof(DateTime))
            {
                converted_property_value = DateTime.Parse((string)value);
            }
            else
            {
                converted_property_value = Convert.ChangeType(value, prop.PropertyType);
            }
            prop.SetValue(target, converted_property_value, null);
        }
    }
}
