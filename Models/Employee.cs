﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrangeTest.Models
{
    public class Employee
    {
        public int id { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public DateTime EmployementDate { get; set; }
    }
}
