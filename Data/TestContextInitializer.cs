﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrangeTest.Models;

namespace OrangeTest.Data
{
    public static class TestContextInitializer
    {
        public static void Initialize(TestContext context)
        {
            context.Database.EnsureCreated();

            if (ShouldInitialize(context))
            {
                FillTable(context);
            }
        }


        public static bool ShouldInitialize(TestContext context)
        {
            return !context.Employees.Any();
        }


        public static void FillTable(TestContext context)
        {
            var employees = new Employee[]
            {
                new Employee{Company = "Airbus",           Position = "Technical Analyst",            Name = "Sam",       Surname = "William",    Age = 35, EmployementDate = DateTime.Parse("2000-01-06")},
                new Employee{Company = "Renault",          Position = "Project Manager",              Name = "Alexander", Surname = "Brown",      Age = 20, EmployementDate = DateTime.Parse("2001-02-05")},
                new Employee{Company = "Siemens",          Position = "Application Support Engineer", Name = "Ryan",      Surname = "Walker",     Age = 39, EmployementDate = DateTime.Parse("2002-03-04")},
                new Employee{Company = "CarGo",            Position = "Flex Developer",               Name = "John",      Surname = "Scott",      Age = 45, EmployementDate = DateTime.Parse("2003-04-03")},
                new Employee{Company = "Pro Bugs",         Position = "Senior C/C++ Developer",       Name = "James",     Surname = "Phillips",   Age = 30, EmployementDate = DateTime.Parse("2004-05-02")},
                new Employee{Company = "IT Consultant",    Position = "UNIX/C++ Developer",           Name = "Brian",     Surname = "Edwards",    Age = 20, EmployementDate = DateTime.Parse("2005-06-01")},
                new Employee{Company = "Europe IT",        Position = "Ruby Developer",               Name = "Jack",      Surname = "Richardson", Age = 20, EmployementDate = DateTime.Parse("2006-07-12")},
                new Employee{Company = "Cisco",            Position = "Frontend Developer",           Name = "Alex",      Surname = "Howard",     Age = 21, EmployementDate = DateTime.Parse("2007-08-11")},
                new Employee{Company = "Hewlett-Packard",  Position = "Node.js Developer",            Name = "Carlos",    Surname = "Woods",      Age = 36, EmployementDate = DateTime.Parse("2008-09-10")},
                new Employee{Company = "Sun Microsystems", Position = "Frontend Developer",           Name = "Adrian",    Surname = "Russel",     Age = 31, EmployementDate = DateTime.Parse("2009-10-09")},
                new Employee{Company = "Big Machines",     Position = "Scala Developer",              Name = "Jeremy",    Surname = "Hamilton",   Age = 30, EmployementDate = DateTime.Parse("2010-11-08")},
                new Employee{Company = "Apple",            Position = "Sales Manager",                Name = "Ivan",      Surname = "Rasputin",   Age = 24, EmployementDate = DateTime.Parse("2011-12-07")},
                new Employee{Company = "Adobe",            Position = "PHP/HTML Developer",           Name = "Peter",     Surname = "West",       Age = 26, EmployementDate = DateTime.Parse("2012-01-06")},
                new Employee{Company = "IBM",              Position = "Designer",                     Name = "Scott",     Surname = "Simpson",    Age = 29, EmployementDate = DateTime.Parse("2013-02-05")},
                new Employee{Company = "Intel",            Position = "Architect",                    Name = "Lorenzo",   Surname = "Tucker",     Age = 39, EmployementDate = DateTime.Parse("2014-03-04")},
                new Employee{Company = "Bridges",          Position = "Engineer",                     Name = "Randy",     Surname = "Grant",      Age = 30, EmployementDate = DateTime.Parse("2015-04-03")},
                new Employee{Company = "Google",           Position = "Analytic",                     Name = "Arthur",    Surname = "Gardner",    Age = 31, EmployementDate = DateTime.Parse("2016-05-02")},
                new Employee{Company = "MIT",              Position = "Unit Testing Developer",       Name = "Orlando",   Surname = "Ruiz",       Age = 30, EmployementDate = DateTime.Parse("2017-06-01")},
            };

            foreach (var employee in employees)
            {
                context.Employees.Add(employee);
            }
            context.SaveChanges();
        }
    }
}
