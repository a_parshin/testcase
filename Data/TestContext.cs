﻿using Microsoft.EntityFrameworkCore;
using OrangeTest.Models;

namespace OrangeTest.Data
{
    public class TestContext : DbContext
    {
        public TestContext(DbContextOptions<TestContext> options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
    }
}
